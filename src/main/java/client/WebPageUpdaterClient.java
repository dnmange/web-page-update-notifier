package client;

import parser.UrlParser;
import service.UrlService;

import java.io.File;
import java.io.IOException;

import java.util.List;

public class WebPageUpdaterClient {

    private String fileName;
    private static final long SLEEP_TIME = 5000;

    public static void main(String args[]) {

        WebPageUpdaterClient webPageUpdaterClient = new WebPageUpdaterClient();
        webPageUpdaterClient.execute();
    }

    public void execute() {

        String fileName = System.getProperty("user.dir") + "/src/main/urls.txt";

        try {
            File file = new File(fileName);
            UrlParser urlParser = new UrlParser();

            List<UrlService> urlServices = urlParser.parse(file);

            while(true){

                for (UrlService urlService: urlServices) {
                    urlService.notifyIfContentUpdated();
                }
                Thread.sleep(SLEEP_TIME);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
