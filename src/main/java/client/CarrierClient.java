package client;

import java.util.HashMap;

public class CarrierClient {

    // mapping from carrier name to their mms gateway
    private static HashMap<String, String> carrierMap = new HashMap<>();

    static {

        carrierMap.put("alltel", "mms.alltelwireless.com");
        carrierMap.put("att", "mms.att.net");
        carrierMap.put("boost", "myboostmobile.com");
        carrierMap.put("cricket", "mms.cricketwireless.net");
        carrierMap.put("fi", "msg.fi.google.com");
        carrierMap.put("uscellular", "mms.uscc.net");
        carrierMap.put("verizon", "vzwpix.com");
        carrierMap.put("sprint", "pm.sprint.com");
        carrierMap.put("virgin","vmpix.com");
        carrierMap.put("tmobile", "tmomail.net");
        carrierMap.put("lyca", "mms.us.lycamobile.com");
    }

    // returns phone in form of email
    public static String getPhoneEmailHost(String carrierName) {
        return carrierMap.get(carrierName);
    }

}
