package parser;

import notify.UrlObserver;
import notify.UrlObserverFactory;
import service.UrlService;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UrlParser {

    // making sure that for one url only one UrlService object is created
    private HashMap<String, UrlService> urlServiceMap;

    public UrlParser() {
        urlServiceMap = new HashMap<>();
    }

    public List<UrlService> parse(File file) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        List<UrlService> urlServices = new ArrayList<>();

        String line;

        while ((line = bufferedReader.readLine()) != null) {

            // remove whitespace
            line = line.trim();

            String[] operands = line.split(" ");

            // store the arguments for email and phone
            String[] argsForObserver = new String[operands.length - 2];

            for (int i = 2; i <operands.length; i++) argsForObserver[i - 2] = operands[i];

            UrlObserver observer = UrlObserverFactory.getUrlObserver(operands[1], argsForObserver);

            // checking for existing URL
            if (urlServiceMap.containsKey(operands[0])) {
                urlServiceMap.get(operands[0]).subscribe(observer);
            }else {
                UrlService urlService = new UrlService(new URL(operands[0]));

                urlService.subscribe(observer);
                urlServices.add(urlService);

                urlServiceMap.put(operands[0], urlService);
            }

        }

        return urlServices;
    }
}
