package notify;

import io.reactivex.disposables.Disposable;
import server.MailServer;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class EmailUrlObserver implements UrlObserver {

    private String toEmail;
    private String fromEmail;
    private Transport transport;
    private MimeMessage mimeMessage;

    public EmailUrlObserver(String[] args) {

        this.toEmail = args[0];
        this.fromEmail = "darshanmange94@gmail.com";

        try {
            this.setTransport(MailServer.getMailSession().getTransport("smtp"));
            this.generateMimeMessage();
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }


    @Override
    public void notifyMessage(String message) {

        try {
            this.getMimeMessage().setText(message);

            transport.connect();
            transport.sendMessage(getMimeMessage(), getMimeMessage().getAllRecipients());

        } catch (Exception excpetion) {
            excpetion.printStackTrace();
        }

    }

    private void generateMimeMessage() {

        try {
            // Create a default MimeMessage object.
            MimeMessage mimeMessage = new MimeMessage(MailServer.getMailSession());

            // set fromEmail in header
            mimeMessage.setFrom(new InternetAddress(fromEmail));

            // set to in header
            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

            // set subject
            mimeMessage.setSubject("Content Update");

            this.setMimeMessage(mimeMessage);

        } catch (MessagingException exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(Object o) {
        if (!(o instanceof String))
            return;
        this.notifyMessage((String) o);
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public MimeMessage getMimeMessage() {
        return mimeMessage;
    }

    public void setMimeMessage(MimeMessage mimeMessage) {
        this.mimeMessage = mimeMessage;
    }
}
