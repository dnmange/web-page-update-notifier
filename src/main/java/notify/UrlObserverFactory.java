package notify;

public class UrlObserverFactory {

    /*
    * Returns Subclass objects based on the notificationMethod paramater
    * */
    public static UrlObserver getUrlObserver(String notifcationMethod, String[] args) {

        if (notifcationMethod.equals("sms"))
            return new PhoneUrlObserver(args);
        else if (notifcationMethod.equals("mail"))
            return new EmailUrlObserver(args);
        else if (notifcationMethod.equals("console"))
            return new ConsoleUrlObserver();

        return null;
    }
}
