package notify;

import io.reactivex.Observer;

public interface UrlObserver extends Observer {

    void notifyMessage(String message);
}
