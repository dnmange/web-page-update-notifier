package notify;

import io.reactivex.disposables.Disposable;

public class ConsoleUrlObserver implements UrlObserver {

    @Override
    public void notifyMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(Object o) {
        if (!(o instanceof String))
            return;

        this.notifyMessage((String) o);
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }
}
