package service;

import io.reactivex.Observer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import notify.EmailUrlObserver;
import notify.PhoneUrlObserver;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class UrlService {

    private URL url;

    // three streams are created in order send notifications simulataneously
    private Subject<String> defaultStream;
    private Subject<String> emailStream;
    private Subject<String> phoneStream;

    private long lastUpdated;

    private UrlService() {

        defaultStream = PublishSubject.create();
        emailStream = PublishSubject.create();
        phoneStream = PublishSubject.create();

        lastUpdated = System.currentTimeMillis();
    }

    public UrlService(URL url) {

        this();
        this.url = url;
    }

    public void subscribe(Observer observer) {

        if (observer instanceof EmailUrlObserver) {
            emailStream.subscribe(observer::onNext, Throwable::printStackTrace);
        } else if (observer instanceof PhoneUrlObserver) {
            phoneStream.subscribe(observer::onNext, Throwable::printStackTrace);
        } else {
            defaultStream.subscribe(observer::onNext, Throwable::printStackTrace);
        }
    }

    public void notifyIfContentUpdated() {

        try {
            long lastModified = getLastModifiedTimeFromServer();

            if (lastModified > lastUpdated) {

                String message =this.getNotificationMessage();

                // triggers observer that are listening to the streams
                defaultStream.onNext(message);
                emailStream.onNext(message);
                phoneStream.onNext(message);

                this.lastUpdated = lastModified;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private long getLastModifiedTimeFromServer() throws IOException{
        URLConnection connect = url.openConnection();
        return connect.getLastModified();
    }

    private URL getUrl() {
        return url;
    }

    private String getNotificationMessage() {
        return "Web content of address: " + url +" is updated";
    }

    /*
     * as URLService is tightly couple on url, equals method should also be dependent on field url
     */
    @Override
    public boolean equals(Object obj) {

        if (obj == null) return false;

        if (!(obj instanceof UrlService)) return false;

        UrlService serviceObj = (UrlService) obj;

        return serviceObj.getUrl().equals(this.url);
    }

    /*
     * Hashcode is overrided so that equals and hashcode are in sync
     * */
    @Override
    public int hashCode() {
        return this.url.hashCode();
    }
}
