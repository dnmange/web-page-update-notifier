import notify.*;

import parser.UrlParser;
import service.UrlService;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

import javax.mail.Transport;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;


public class WebPageUpdaterTest {

    @Test
    public void testObserverPattern() throws IOException {

        HttpURLConnection mockHttp = mock(HttpURLConnection.class);
        when(mockHttp.getResponseCode()).thenReturn(200);

        when(mockHttp.getLastModified()).thenReturn(1244416302499L)
                .thenReturn(2244416302499L)
                .thenReturn(3244416302499L)
                .thenReturn(244416302499L);

        String spec = "http://www.eli.sdsu.edu/courses/fall18/cs635/notes/index.html";

        URL url = MockUrl.createUrl(spec, mockHttp);
        UrlService urlService = new UrlService(url);
        UrlObserver observer = mock(UrlObserver.class);

        urlService.subscribe(observer); // attaching observer

        String message = "Web content of address: " + url.toString() +" is updated";

        urlService.notifyIfContentUpdated();
        urlService.notifyIfContentUpdated();
        urlService.notifyIfContentUpdated();
        urlService.notifyIfContentUpdated();

        // observer onNext is attached when urlService notifies its change
        // hence, we put a verify on Observer's onNext
        verify(observer, times(2)).onNext(message);
    }

    @Test
    public void testNotification() throws IOException{

        HttpURLConnection mockHttp = mock(HttpURLConnection.class);
        when(mockHttp.getResponseCode()).thenReturn(200);

        when(mockHttp.getLastModified()).thenReturn(3244416302499L);

        String spec = "http://www.eli.sdsu.edu/courses/fall18/cs635/notes/index.html";

        URL url = MockUrl.createUrl(spec, mockHttp);
        UrlService urlService = new UrlService(url);

        String[] argsSms = new String[] {"6196840719", "sprint"};
        String[] argsEmail = new String[] {"dnmange@sdsu.edu"};

        UrlObserver observer1 = UrlObserverFactory.getUrlObserver("console", null);
        UrlObserver observer2 = UrlObserverFactory.getUrlObserver("sms", argsSms);
        UrlObserver observer3 = UrlObserverFactory.getUrlObserver("mail", argsEmail);

        urlService.subscribe(observer1);
        urlService.subscribe(observer2);
        urlService.subscribe(observer3);

        // checks whether observer created by factory are correct
        assertThat(observer1, instanceOf(ConsoleUrlObserver.class));
        assertThat(observer2, instanceOf(PhoneUrlObserver.class));
        assertThat(observer3, instanceOf(EmailUrlObserver.class));

        urlService.notifyIfContentUpdated();
    }

    @Test
    public void testEmail() throws Exception {

        EmailUrlObserver urlObserver = new EmailUrlObserver(new String[]{"dnmange@sdsu.edu"});

        // transport is mocked as to verify whether sending an email is working or not
        Transport transport = mock(Transport.class);

        urlObserver.setTransport(transport);

        urlObserver.notifyMessage("Test");

        verify(transport, times(1))
                .sendMessage(urlObserver.getMimeMessage(), urlObserver
                                                                .getMimeMessage()
                                                                .getAllRecipients());
    }

    @Test
    public void testPhone() throws Exception {

        PhoneUrlObserver urlObserver = new PhoneUrlObserver(new String[]{"6196840719", "sprint"});

        // transport is mocked as to verify whether sending a message to phone is working or not
        Transport transport = mock(Transport.class);
        urlObserver.setTransport(transport);

        urlObserver.notifyMessage("Test");

        verify(transport, times(1))
                .sendMessage(urlObserver.getMimeMessage(), urlObserver
                                                                .getMimeMessage()
                                                                .getAllRecipients());
    }

    @Test
    public void testParser() throws IOException {

        String fileName = System.getProperty("user.dir") + "/src/main/urls.txt";

        List<UrlService> expectedList = new ArrayList<>();

        UrlService urlService1 = new UrlService(new URL("http://www.eli.sdsu.edu/courses/fall18/cs635/notes/index.html"));
        UrlService urlService2 = new UrlService(new URL("http://www.eli.sdsu.edu/index.html"));

        expectedList.add(urlService1);
        expectedList.add(urlService2);

        UrlParser parser = new UrlParser();

        List<UrlService> actualList = parser.parse(new File(fileName));

        assertEquals(expectedList.size(), actualList.size());

        assertArrayEquals(expectedList.toArray(), actualList.toArray());
    }

}
