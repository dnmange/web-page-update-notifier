import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class MockUrl {

    public static URL createUrl(String spec, HttpURLConnection connection) {

        URL url = null;
        try {
            URL context = new URL(spec);
            URLStreamHandler handler = new URLStreamHandler() {
                @Override
                protected URLConnection openConnection(URL u) {

                    return connection;
                }
            };

            url = new URL(context, spec, handler);
        } catch (Exception excpetion) {
            excpetion.printStackTrace();
        }

        return url;
    }
}
