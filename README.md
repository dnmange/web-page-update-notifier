## Abstract
---

There are times that one would like to know when a web page has been updated. For example
it would be nice to know when the assignment page for a course is changed. Users can be notified by updates via email, text and also can be printed out on the 
console. The main purpose of this application is to learn how observer patten can be implemented with reactive Java. Another aspect of this application is to handle dynamic changes in the 
system without changing the existing code. Factory pattern has been used to address this dynamic change.

    
## Usage
---

Step1 : Clone this repo, and do ``cd src/main/java/client``   
Step2 : Run ``WebPageUpdaterClient``